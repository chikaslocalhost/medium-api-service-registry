FROM openjdk:8-jdk-alpine
LABEL maintainer="sendtoclabz@gmail.com"
VOLUME /tmp
ADD build/libs/medium-api-service-registry-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 3000
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
