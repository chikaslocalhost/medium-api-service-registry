package com.clabz.medium;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer //Enabling Eureka Server
public class MediumApiServiceRegistryApplication {

    public static void main(String[] args) {
        SpringApplication.run(MediumApiServiceRegistryApplication.class, args);
    }
}
